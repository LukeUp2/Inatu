import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {

  },
  button: {
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: "#fff",
    flex: 1
  }
});