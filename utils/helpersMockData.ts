import moment from "moment";

export const helpersData = [
  {
    name: "Miles",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Gwen",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Anakin",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Luke",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Hermione",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Gon",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Harry",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
  {
    name: "Padme",
    ssn: "00000023403",
    address: "Rua baugulho",
    birthDate: moment(new Date()).toISOString(true),
    surname: "apelido",
  },
];
