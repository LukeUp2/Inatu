export const collectData = [
  {
    year: "2023",
    data: [
      { quantity: 76.6, name: "Buriti", endDate: "28/02/2023", sufix: "kg" },
      { quantity: 23.4, name: "Açaí", endDate: "31/01/2023", sufix: "kg" },
      { quantity: 39.3, name: "Copaíba", endDate: "01/01/2023", sufix: "L" },
    ],
  },
  {
    year: "2022",
    data: [
      { quantity: 45.4, name: "Cacau", endDate: "28/12/2022", sufix: "kg" },
      { quantity: 23.4, name: "Castanha", endDate: "31/05/2022", sufix: "kg" },
      { quantity: 39.3, name: "Copaíba", endDate: "07/09/2022", sufix: "L" },
    ],
  },
  {
    year: "2021",
    data: [
      { quantity: 54.6, name: "Buriti", endDate: "28/02/2021", sufix: "kg" },
      { quantity: 21.4, name: "Açaí", endDate: "31/01/2021", sufix: "kg" },
      { quantity: 19.3, name: "Copaíba", endDate: "01/01/2021", sufix: "L" },
    ],
  },
];
