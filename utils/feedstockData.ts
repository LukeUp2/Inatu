import Acai from "../assets/feedstockIcons/acai.svg";
import Andiroba from "../assets/feedstockIcons/andiroba.svg";
import Breu from "../assets/feedstockIcons/breu.svg";
import Buriti from "../assets/feedstockIcons/buriti.svg";
import Cacau from "../assets/feedstockIcons/cacau.svg";
import CafeVerde from "../assets/feedstockIcons/cafe grao.svg";
import CastanhaAmd from "../assets/feedstockIcons/castanha amd.svg";
import CastanhaOur from "../assets/feedstockIcons/our castanha.svg";
import Citronela from "../assets/feedstockIcons/citronela.svg";
import Copaiba from "../assets/feedstockIcons/copaiba.svg";
import Cumaru from "../assets/feedstockIcons/cumaru.svg";
import Cupuacu from "../assets/feedstockIcons/cupu.svg";
import Latex from "../assets/feedstockIcons/latex.svg";
import Murumuru from "../assets/feedstockIcons/muru.svg";
import Pataua from "../assets/feedstockIcons/pataua.svg";
import PauRosa from "../assets/feedstockIcons/pau rosa.svg";
import PimentaMacaco from "../assets/feedstockIcons/pim macaco.svg";
import Pitanga from "../assets/feedstockIcons/pitanga.svg";
import Priprioca from "../assets/feedstockIcons/priprioca.svg";
import SangueDragao from "../assets/feedstockIcons/sangue drag.svg";
import TucumaAmd from "../assets/feedstockIcons/tucuma amd.svg";
import TucumaSem from "../assets/feedstockIcons/tucuma sem.svg";
import Uccuba from "../assets/feedstockIcons/ucuuba.svg";

export const feedstockData = [
  {
    id: "1",
    name: "Açaí",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "2",
    name: "Andiroba",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "3",
    name: "Breu",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "4",
    name: "Buriti",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "5",
    name: "Cacau",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "6",
    name: "Café verde",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "7",
    name: "Castanha - Amêndoa",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "8",
    name: "Castanha - Ouriço",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "9",
    name: "Citronela",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "10",
    name: "Copaíba",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "11",
    name: "Cumaru",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "12",
    name: "Cupuaçu",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "13",
    name: "Látex",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "14",
    name: "Murumuru",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "15",
    name: "Patauá",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "16",
    name: "Pau rosa",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "17",
    name: "Pimenta de macaco",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "18",
    name: "Pitanga",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "19",
    name: "Priprioca",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "20",
    name: "Sangue de dragão",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "21",
    name: "Tucumã - Amêndoa",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "local",
  },
  {
    id: "22",
    name: "Tucumã - Semente",
    category: "grupo",
    standardMeasure: "quilos",
    collectType: "viagem",
  },
  {
    id: "23",
    name: "Uccuba",
    category: "isolada",
    standardMeasure: "quilos",
    collectType: "local",
  },
];

export const feedstockIcons = {
  Açaí: Acai,
  Andiroba: Andiroba,
  Breu: Breu,
  Buriti: Buriti,
  Cacau: Cacau,
  "Café verde": CafeVerde,
  "Castanha - Amêndoa": CastanhaAmd,
  "Castanha - Ouriço": CastanhaOur,
  Citronela: Citronela,
  Copaíba: Copaiba,
  Cumaru: Cumaru,
  Cupuaçu: Cupuacu,
  Látex: Latex,
  Murumuru: Murumuru,
  Patauá: Pataua,
  "Pau rosa": PauRosa,
  "Pimenta de macaco": PimentaMacaco,
  Pitanga: Pitanga,
  Priprioca: Priprioca,
  "Sangue de dragão": SangueDragao,
  "Tucumã - Amêndoa": TucumaAmd,
  "Tucumã - Semente": TucumaSem,
  Uccuba: Uccuba,
};
