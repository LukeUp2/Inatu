export const pdfData = [
  {
    title: "Derruba direcionada e beneficiamento da madeira",
    uri: "https://drive.google.com/file/d/1hUV-A5-YNoVdd9TNE2H64t7MYDXpGP0E/view?usp=sharing",
  },
  {
    title: "Gestão florestal madeireira",
    uri: "https://drive.google.com/file/d/1yhJVyK-SPokAbhz4yWgWG0jGstBO9vjE/view?usp=sharing",
  },
  {
    title: "Gestão florestal não madeireira",
    uri: "https://drive.google.com/file/d/1Enh7y4fXRegq2anmvBVdmzg99Z2i98Rz/view?usp=sharing",
  },
  {
    title:
      "Guia prático do manejo florestal comunitário e familiar no Amazonas",
    uri: "https://drive.google.com/file/d/1Wu1HZ12VRUMd7GFSAM_nhYHGvKIgDPi_/view?usp=sharing",
  },
  {
    title:
      "Operação de máquinas, equipamentos e segurança na atividade florestal",
    uri: "https://drive.google.com/file/d/1xkCYA4WBIbxR_OOTH8EpSeL31wnMwjB1/view?usp=sharing",
  },
  {
    title: "Passo a passo Cadastro Ambiental Rural",
    uri: "https://drive.google.com/file/d/1v8Iy1JQIQhYe6vU-5rf20tGU0Cn58Mjh/view?usp=sharing",
  },
  {
    title: "Passo a passo Cadastro Técnico Federal",
    uri: "https://drive.google.com/file/d/1p2ceHDAXyxucpniCYr7T4itI5gFn_LA7/view?usp=sharing",
  },
  {
    title: "Passo a Passo Certificado de Cadastro de Imóvel Rural - CCIR",
    uri: "https://drive.google.com/file/d/12hz3XXE9tNyv8GYOIeb6qjWvRpbA5zbQ/view?usp=sharing",
  },
];
