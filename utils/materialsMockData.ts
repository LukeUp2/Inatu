export const materialData = [
    {
        category: 'Obrigatórios',
        data: [
            { name: "Par de botas", icon: "#00491E", quantity: '0', price: 80.00 },
            { name: "Capacete", icon: "#ff6b01", quantity: '0', price: 12.99 },
            { name: "Perneira", icon: "#e21f41", quantity: '0', price: 12.99 },
            { name: "Colete luminoso", icon: "#C81DE4", quantity: '0', price: 43.99 },
            { name: "Par de botas", icon: "#00491E", quantity: '0', price: 15.00 },
            { name: "Capacete", icon: "#ff6b01", quantity: '0', price: 34.99 },
            { name: "Perneira", icon: "#e21f41", quantity: '0', price: 10.00 },
            { name: "Colete luminoso", icon: "#C81DE4", quantity: '0', price: 80.00 },
        ]
    },
    {
        category: 'Opcionais',
        data: [
            { name: "Par de botas", icon: "#00491E", quantity: '0', price: 30.00 },
            { name: "Capacete", icon: "#C81DE4", quantity: '0', price: 12.99 },
            { name: "Perneira", icon: "#00491E", quantity: '0', price: 34.99 },
            { name: "Colete luminoso", icon: "#ff6b01", quantity: '0', price: 60.00 },
        ]
    },
]

export const materialObject = [
    { name: 'trado_3/4', unitPrice: 100.00, lifespan: 10, label: 'Trado 3/4' },
    { name: 'mangueira_de_borracha_3/4m', unitPrice: 50.00, lifespan: 2, label: 'Mangueira de borracha 3/4m' },
    { name: 'cano_de_pvc_1/2_6m', unitPrice: 40.00, lifespan: 4, label: 'Cano de PVC 1/2 6m' },
    { name: 'tampa_cano_pvc_1/2', unitPrice: 3.00, lifespan: 4, label: 'Tampa de cano PVC 1/2' },
    { name: 'funil', unitPrice: 30.00, lifespan: 5, label: 'Funil' },
    { name: 'bombona_5L_ou_vasilhame_escuro_com_tampa', unitPrice: 12.00, lifespan: 3, label: 'Bombona 5L ou vasilhame escuro com tampa' },
    { name: 'terçado_ou_foice_com_cabo_alongado', unitPrice: 120.00, lifespan: 6, label: 'Terçado ou foice com cabo alongado' },
    { name: 'sacos_de_rafia_50kg', unitPrice: 5.00, lifespan: 4, label: 'Sacos de ráfia 50kg' },
    { name: 'lona_plástica_ou_tela_tipo_sombrite_4x20m', unitPrice: 500.00, lifespan: 4, label: 'Lona plástica ou tela tipo sombrite (4x20m)' },
    { name: 'peçonha_ou_equipamento_de_rapel', unitPrice: 400.00, lifespan: 5, label: 'Peçonha ou equipamento de rapel' }, 
    { name: 'serrote', unitPrice: 50.00, lifespan: 6, label: 'Serrote' },
    { name: 'capacete', unitPrice: 40.00, lifespan: 5, label: 'Capacete' },
    { name: 'bota', unitPrice: 100.00, lifespan: 3, label: 'Bota' },
]