export const theme = {
    colors: {
        brown_dark: '#653024',
        brown_light: '#c09668',
        brown_super_light: '#F6EBDF',
        beige: '#D6A461',
        earth: '#BE6A14',
        golden: '#FFA400',
        green_dark: '#00491E',
        green_regular: '#9CAF88',
        green_light: '#EDF6E0',
        grayscale: {
            900: '#151313',
            800: '#534b4b',
            500: '#958a8a',
            300: '#CAC4C4',
            100: '#f2f1f3',
        },
        white: '#fff',
        orange: '#ff6b01',
        yellow: '#FFD600',
        yellow_light: '#fef7cb',
        violet: '#C81DE4',
        red: '#e21f41',
        red_light: '#fff1f1'
    },
    font: {
        bw_bold: 'BWMitga-Bold',
        bw_regular: 'BWMitga-Regular',
        archivo_bold: 'Archivo-Bold',
        archivo_regular: 'Archivo-Regular',
        basicSans_regular: 'BasicSans-Regular',
        basicSans_bold: 'BasicSans-Bold',
        bernhard_regular: 'BernhardGothic-Regular',
        bernhard_bold: 'BernhardGothic-Bold',
    }

}